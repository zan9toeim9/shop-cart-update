
## Run Project

1. Clone project from repository.
2. Install all necessary packages **npm install**.
3. Run project **npm start**.

---

## Technical test description / initial data

You could find the test description and other test project initial data in **extra files** directory.

---

## Additional information

1. [json-server](https://github.com/typicode/json-server) is used for server simulation.
2. For changing server request interval and dollar rate use according inputs. 
3. Catalog is located on catalog page and cart on cart page.
4. Feel free to contact me if you have any questions.
