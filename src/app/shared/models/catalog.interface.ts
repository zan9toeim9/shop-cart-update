interface Catalogs {
  [key: string]: Catalog;
}

interface Catalog {
  G: 'string';
  C: number;
  B: {
    [key: string]: MapGood;
  };
}

interface MapGood {
  N: string;
  T: string | number;
}

export { Catalogs, Catalog, MapGood };
