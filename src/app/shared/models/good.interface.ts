interface Good {
  B: boolean;
  C: number;
  CV: any;
  G: number;
  P: number;
  Pl: any;
  T: number;
}

interface ViewGood {
  categoryName: string;
  categoryId: number;
  goodName: string;
  goodId: number;
  total: number;
  price: number;
  basePrice: number;
  priceStatus: string;
}

export { Good, ViewGood };
