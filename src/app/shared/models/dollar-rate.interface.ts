export interface DollarRate {
  rate: number;
  status: string;
}
