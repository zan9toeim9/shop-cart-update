import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Good } from '../models/good.interface';
import { Catalogs } from '../models/catalog.interface';

interface ResponseCatalog {
  Error: string;
  Id: number;
  Success: boolean;
  Value: {
    Goods: Good[];
  };
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  // fake server host
  private readonly serverURL = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getCatalog(): Observable<any> {
    return this.http.get<ResponseCatalog>(`${this.serverURL}/catalog`);
  }

  getGoodsMapping(): Observable<any> {
    return this.http.get<Catalogs>(`${this.serverURL}/goods`);
  }

}
