import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DollarRate } from '../models/dollar-rate.interface';

@Injectable({
  providedIn: 'root'
})
export class CurrencyRateService {

  private dollarRate = new BehaviorSubject<DollarRate>({ rate: 70, status: 'stable' });
  dollarRate$ = this.dollarRate.asObservable();

  constructor() { }

  // dollar rate status is needed for displaying respective cell color
  setDollarRateValue(value: number): void {
    let dollarRateStatus = 'stable';
    if (this.dollarRate.value.rate > value) {
      dollarRateStatus = 'decreased';
    } else if (this.dollarRate.value.rate < value) {
      dollarRateStatus = 'increased';
    } else {
      dollarRateStatus = 'stable';
    }
    this.dollarRate.next({ rate: value, status: dollarRateStatus });
  }

  clearDollarRateStatus(): void {
    this.dollarRate.next({ rate: this.dollarRate.value.rate, status: 'stable' });
  }

}
