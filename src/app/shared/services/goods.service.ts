// this service contains all common methods and data related to goods/catalog

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ApiService } from './api.service';
import { Catalogs } from '../models/catalog.interface';
import { ViewGood } from '../models/good.interface';

@Injectable({
  providedIn: 'root'
})
export class GoodsService {
  private goodsMapper: Catalogs = {};
  private cartGoods = {};
  private cartTotal = 0;
  private updatingCartItem = new BehaviorSubject<number>(0);

  updatingCartItem$ = this.updatingCartItem.asObservable();

  constructor(private apiService: ApiService) { }

  get goodsInCart(): any {
    return this.cartGoods;
  }

  // this method maps "backend" api model to view model (for displaying in catalog and cart component)
  mapGoodsToViewModel(goods, dollarRateValue: number): Observable<ViewGood[]> {
    // observable for updating model
    const goodsMapper$ = Object.keys(this.goodsMapper).length ? of(this.goodsMapper) : this.apiService.getGoodsMapping();
    return goodsMapper$
      .pipe(
        map((goodsMapper: Catalogs) => {
            this.goodsMapper = goodsMapper;
            return goods.map((good) => ({
              categoryName: goodsMapper[good.G].G,
              categoryId: good.G,
              goodName: goodsMapper[good.G].B[good.T].N,
              goodId: good.T,
              total: good.P,
              price: parseFloat(Number(good.C * dollarRateValue).toFixed(2)),
              basePrice: good.C,
              priceStatus: 'stable',
            }));
          }
        ),
        take(1)
      );
  }

  // updating cart items (removing, adding goods)
  updateCartItems(cartGood, remove = false): any {

    if (remove) {
      delete this.cartGoods[cartGood.good.goodId];
      this.cartTotal -= cartGood.amount;
    } else {
      let previousAmount = 0;
      if (this.cartGoods[cartGood.good.goodId]) {
        previousAmount = this.cartGoods[cartGood.good.goodId].amount;
      }

      this.cartTotal += (cartGood.amount - previousAmount);
      this.cartGoods[cartGood.good.goodId] = {...cartGood};
    }

    this.updatingCartItem.next(this.cartTotal);

    // update cart in local storage
    if (this.cartTotal) {
      localStorage.setItem('cart', JSON.stringify(this.cartGoods));
      localStorage.setItem('cartTotal', JSON.stringify(this.cartTotal));
    } else {
      localStorage.removeItem('cart');
      localStorage.removeItem('cartTotal');
    }

    return this.cartGoods;
  }

  // update cart price on dollar rate changing
  updateCartItemsPrice(dollarRateValue): any {
    for (const key in this.cartGoods) {
      if (this.cartGoods[key]) {
        this.cartGoods[key].good.price = Number(this.cartGoods[key].good.basePrice * dollarRateValue).toFixed(2);
      }
    }
    localStorage.setItem('cart', JSON.stringify(this.cartGoods));
    return this.cartGoods;
  }

  addItemToCart(good): boolean {
    let cartGood = this.cartGoods[good.goodId] && {...this.cartGoods[good.goodId]};

    if (cartGood && cartGood.amount < good.total) {
      cartGood.amount += 1;
    } else if (!cartGood) {
      cartGood = { good, amount: 1 };
    } else {
      return false;
    }
    this.updateCartItems(cartGood);
    return true;
  }

  // restoring cart goods form local storage
  restoreCartFromStorage(): void {
    const localStorageCart = JSON.parse(localStorage.getItem('cart'));
    const cartTotal = Number(localStorage.getItem('cartTotal'));
    if (localStorageCart) {
      this.cartTotal = cartTotal;
      this.updatingCartItem.next(this.cartTotal);
      this.cartGoods = localStorageCart;
    }
  }
}
