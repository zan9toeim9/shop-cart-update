import {Component, Input, OnInit} from '@angular/core';
import {GoodsService} from '../../shared/services/goods.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent {

  isTableOpened = true;

  @Input() category;

  constructor(private goodsService: GoodsService) { }

  triggerTable(): void {
    this.isTableOpened = !this.isTableOpened;
  }

  addToCart(good): void {
    this.goodsService.addItemToCart(good);
  }
}
