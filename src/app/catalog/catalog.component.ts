import {Component, OnDestroy, OnInit} from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { interval, Subscription } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { GoodsService } from '../shared/services/goods.service';
import { CurrencyRateService } from '../shared/services/currency-rate.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DollarRate } from '../shared/models/dollar-rate.interface';
import {ViewGood} from '../shared/models/good.interface';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit, OnDestroy {
  catalogGoods = {};
  catalogUpdateInterval = 15000;
  catalogUpdateForm: FormGroup;
  showIntervalUpdatedMessage = false;
  showIntervalErrorMessage = false;
  dollarRate: DollarRate;

  currencyRateSub$: Subscription;
  serverDataSub$: Subscription;

  constructor(private apiService: ApiService,
              private goodsService: GoodsService,
              private currencyRate: CurrencyRateService,
              private  formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createServerDataIntervalSub();
    this.catalogUpdateForm = this.formBuilder.group({interval: this.catalogUpdateInterval});
    this.currencyRateSub$ = this.currencyRate.dollarRate$
      .subscribe((dollarRate) => {
        this.dollarRate = dollarRate;
      });
  }

  ngOnDestroy(): void {
    this.currencyRateSub$.unsubscribe();
    this.serverDataSub$.unsubscribe();
  }

  // every N second send request to server for getting updated catalog model
  createServerDataIntervalSub(): void {
    this.serverDataSub$ = interval(this.catalogUpdateInterval)
      .pipe(
        startWith(0),
        switchMap(() => this.apiService.getCatalog()),
        switchMap((catalog) => this.goodsService.mapGoodsToViewModel(catalog.Value.Goods, this.dollarRate.rate)),
        map((goods: ViewGood[]) => {
          const catalogGoods = {};
          goods.forEach((good) => {
            good.priceStatus = this.dollarRate.status;

            if (!catalogGoods[good.categoryName]) {
              catalogGoods[good.categoryName] = [];
            }
            catalogGoods[good.categoryName].push(good);
          });
          this.currencyRate.clearDollarRateStatus();
          return catalogGoods;
        }),
      )
      .subscribe((catalogGoods) => {
        this.catalogGoods = catalogGoods;
      });
  }

  // change interval for polling fake server
  updateIntervalValue(): void {
    const newInterval = this.catalogUpdateForm.value.interval;
    if (isNaN(newInterval) || newInterval < 2000 || newInterval > 15000) {
      this.showIntervalErrorMessage = true;
    } else {
      this.showIntervalUpdatedMessage = true;
      this.catalogUpdateInterval = newInterval;
      this.serverDataSub$.unsubscribe();
      this.createServerDataIntervalSub();
    }

    setTimeout(() => {
      this.showIntervalUpdatedMessage = false;
      this.showIntervalErrorMessage = false;
    }, 1500);
  }

}
