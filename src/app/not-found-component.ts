import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h2 style="text-align: center">
               404 - Page not found
            </h2>`
})
export class NotFoundComponent {

}
