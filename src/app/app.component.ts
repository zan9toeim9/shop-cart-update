import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GoodsService } from './shared/services/goods.service';
import { CurrencyRateService } from './shared/services/currency-rate.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  dollarRate: number;
  dollarRateForm: FormGroup;
  showDollarRateUpdatedMessage = false;
  showDollarRateErrorMessage = false;
  cartTotal = 0;
  updatingCartSub$: Subscription;

  constructor(private formBuilder: FormBuilder,
              private goodsService: GoodsService,
              private currencyRateService: CurrencyRateService) {}

  // component init hook
  ngOnInit(): void {
    // subscription for updating cart total value
    this.updatingCartSub$ = this.goodsService.updatingCartItem$
      .subscribe((cartTotal) => {
        this.cartTotal = cartTotal;
      });

    // getting cart items on init app (if they exist)
    this.goodsService.restoreCartFromStorage();
    this.dollarRateForm = this.formBuilder.group({ dollarRate: 70 });
  }

  ngOnDestroy(): void {
    this.updatingCartSub$.unsubscribe();
  }

  // manual updating dollar rate reflects on goods price
  updateDollarRate(): void {
    const newDollarRate = this.dollarRateForm.value.dollarRate;
    if (isNaN(newDollarRate) || newDollarRate < 20 || newDollarRate > 80) {
      this.showDollarRateErrorMessage = true;
    } else {
      this.currencyRateService.setDollarRateValue(newDollarRate);
      this.showDollarRateUpdatedMessage = true;
    }

    // showing updating/error message for 1500 ms
    setTimeout(() => {
      this.showDollarRateUpdatedMessage = false;
      this.showDollarRateErrorMessage = false;
    }, 1500);
  }
}
