import {Component, OnDestroy, OnInit} from '@angular/core';
import { GoodsService } from '../shared/services/goods.service';
import {CurrencyRateService} from '../shared/services/currency-rate.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {

  tableHeader = [{name: 'Наименование товара', class: 'cart-table__header-cell--400'},
    {name: 'Количество', class: 'cart-table__header-cell--100'},
    {name: 'Цена', class: 'cart-table__header-cell--150'},
    {name: '', class: 'cart-table__header-cell--100'}];

  cartGoods = {};
  cartTotal = 0;
  totalSum = 0;
  dollarRateSub$: Subscription;
  updateCartItemSub$: Subscription;

  constructor(private goodsService: GoodsService,
              private currencyRateService: CurrencyRateService) { }

  ngOnInit(): void {
    this.cartGoods = this.goodsService.goodsInCart;

    this.updateCartItemSub$ = this.goodsService.updatingCartItem$
      .subscribe((cartTotal) => {
        this.cartTotal = cartTotal;
      });

    // update cart items prices on dollar rate change
    this.dollarRateSub$ = this.currencyRateService.dollarRate$
      .subscribe((dollarRate) => {
        if (!this.cartTotal) {
          this.dollarRateSub$.unsubscribe();
          return;
        }
        this.cartGoods = this.goodsService.updateCartItemsPrice(dollarRate.rate);
        this.updateTotalSum();
      });
  }

  // break down subscription on component destroying
  ngOnDestroy(): void {
    this.dollarRateSub$.unsubscribe();
    this.updateCartItemSub$.unsubscribe();
  }

  // update total sum and total cart items on changing good amount
  changeGoodAmount($event, goodId): void {
    const inputValue = $event.target.value;
    if (inputValue >= 1 && inputValue !== this.cartGoods[goodId].amount) {
      const cartGood = {...this.cartGoods[goodId]};
      let newAmount = inputValue;
      if (inputValue > this.cartGoods[goodId].good.total) {
        newAmount = this.cartGoods[goodId].good.total;
      }
      cartGood.amount = newAmount;
      this.cartGoods = this.goodsService.updateCartItems(cartGood);
    }
    $event.target.value = this.cartGoods[goodId].amount;
    this.updateTotalSum();
  }

  updateTotalSum(): void {
    let totalSum = 0;
    for (const key in this.cartGoods) {
      if (this.cartGoods[key]) {
        totalSum += this.cartGoods[key].amount * this.cartGoods[key].good.price;
      }
    }
    this.totalSum = Number(totalSum.toFixed(2));
  }

  removeGood(goodId): void {
    this.cartGoods = this.goodsService.updateCartItems(this.cartGoods[goodId], true);
    this.updateTotalSum();
  }
}
